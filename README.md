# salary-negotiator
Simple SPA for salary negotiation. The company enters the maximum salary it wants to pay to Prospect. Prospect enters minimum wage he would be willing to work for.
State of the art machine learning engine will then determine if an offer was successful.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```
