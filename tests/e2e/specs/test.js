// https://docs.cypress.io/api/introduction/api.html
const submitForm = (prospectSalary, employerSalary) =>
  cy
    .get('.tab').contains('prospect')
    .click()
    .get('.form__input')
    .type(`${prospectSalary}{enter}`)
    .get('.tab--isSelected')
    .should('contain', 'employer')

    .get('.form__input')
    .clear()
    .type(`${employerSalary}{enter}`);

describe('Switch Tab', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Switches Tab', () => {
    const selectedTabClass = 'tab--isSelected';
    cy
      .get('.tab')
      .eq(1)
      .click()
      .should('have.class', selectedTabClass);
  });

  it('Tests form input', () => {
    const formClasses = {
      valid: 'form__input--valid',
      empty: 'form__input--empty',
    };

    cy
      .get('.form__input')
      .type(1)
      .should('have.class', formClasses.valid)
      .clear()
      .should('have.class', formClasses.empty)
      .type(1);
  });

  it('Successfuly submits', () => {
    const prospectSalary = 1;
    const employerSalary = 2;

    submitForm(prospectSalary, employerSalary)
      .get('.app')
      .should('have.class', 'app--success');
  });

  it('Fail submits', () => {
    const prospectSalary = 2;
    const employerSalary = 1;

    submitForm(prospectSalary, employerSalary)
      .get('.app')
      .should('have.class', 'app--fail');
  });
});
